package com.brevitaz;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.sql.*;

public class AddDataServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException,ServletException{
		String s=req.getParameter("viewsOfSomeoneAboutTopic");
		req.setAttribute("blogData",s);
		
		//req.setAttribute("result",rs);
		res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println("Beer selection advise "+s);
		
		 RequestDispatcher dis=req.getRequestDispatcher("/dataBaseConnectorServlet");          
	     dis.forward(req, res);
	
	}

}
