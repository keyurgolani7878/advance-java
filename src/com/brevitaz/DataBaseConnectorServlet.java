package com.brevitaz;
import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DataBaseConnectorServlet
 */
//@WebServlet("/DataBaseConnectorServlet")
public class DataBaseConnectorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;    
    	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String url="jdbc:mysql://localhost:3306/techblogdata";
        String uName="root";
        String password="password";
        String query="select blog from blogs ";
        String query2="insert into blogs(blog) values(?)";
        String s=null;
        ResultSet resultSet=null;
        try{
        	Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection=DriverManager.getConnection(url,uName,password);
            PreparedStatement statement=connection.prepareStatement(query2);
            s=(String)request.getAttribute("blogData");
            if(s.length()!=0){
            	statement.setString(1,s);
                int count=statement.executeUpdate();
                System.out.println(count);
                resultSet=statement.executeQuery(query);
                request.setAttribute("resulFromDatabase", resultSet);
                ResultSet rs=(ResultSet)request.getAttribute("resulFromDatabase");
                RequestDispatcher dis=request.getRequestDispatcher("/blogsFromDatabase.jsp");          
                dis.forward(request, response);
            }else{
            	RequestDispatcher dis=request.getRequestDispatcher("/nullBlogError.jsp");          
                dis.forward(request, response);
            }
            
            //statement.close();
            //connection.close();
	
        }catch(Exception e){
        	System.out.println("error in second servlets");
        	e.printStackTrace();
        }
        
        
	}

}
